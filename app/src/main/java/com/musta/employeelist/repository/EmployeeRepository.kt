package com.musta.employeelist.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.musta.employeelist.dao.EmployeeDao
import com.musta.employeelist.db.EmployeeRoomDatabase
import com.musta.employeelist.model.Employee

class EmployeeRepository(application: Application) {
    private val employeeDao: EmployeeDao?

    //Observed LiveData notifies the observer :: queries will be executed in separate thread
    val allEmployeeList: LiveData<List<Employee>>

    //This will be run in a non-UI thread or will be thrown an exception
    fun insert(employee: Employee?) {
        EmployeeRoomDatabase.databaseWriterExecutor.execute { employeeDao!!.insert(employee) }
    }

    fun insertDB(employees: List<Employee>?) {
        EmployeeRoomDatabase.databaseWriterExecutor.execute { employeeDao!!.insertAll(employees) }
    }

    //This will be run in a non-UI thread or will be thrown an exception
    fun remove(employee: Employee) {
        EmployeeRoomDatabase.databaseWriterExecutor.execute { employeeDao!!.delete(employee) }
    }

    fun update(employee: Employee) {
        EmployeeRoomDatabase.databaseWriterExecutor.execute { employeeDao!!.update(employee) }
    }

    fun findById(id: Int): LiveData<Employee> {
        return employeeDao!!.findById(id)
    }

    fun deleteAll() {
        return EmployeeRoomDatabase.databaseWriterExecutor.execute { employeeDao!!.deleteAll() }
    }

    init {
        val db: EmployeeRoomDatabase = EmployeeRoomDatabase.getDatabase(application)
        employeeDao = db.employeeDao()
        allEmployeeList = employeeDao.alphabetizedEmployeeList
    }
}