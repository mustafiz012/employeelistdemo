package com.musta.employeelist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.musta.employeelist.R
import com.musta.employeelist.adapter.EmployeeListAdapter.EmployeeViewHolder
import com.musta.employeelist.databinding.ItemEmployeeBinding
import com.musta.employeelist.model.Employee
import com.musta.employeelist.utils.setBitmapImage

class EmployeeListAdapter(private val onClick: (Employee, Int, Int) -> Unit) : RecyclerView.Adapter<EmployeeViewHolder>() {
    private var employeeList: List<Employee>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        return EmployeeViewHolder(ItemEmployeeBinding.inflate(LayoutInflater.from(parent.context)), onClick)
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        employeeList?.get(position)?.let {
            holder.bind(it, position)
        }
    }

    fun setList(employeeList: List<Employee>) {
        this.employeeList = employeeList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (employeeList != null) employeeList!!.size else 0
    }

    class EmployeeViewHolder(private val binding: ItemEmployeeBinding, private var onClick: (Employee, Int, Int) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun bind(employee: Employee, position: Int) {
            binding.tvName.text = employee.name
            "Age: ${employee.age}".also { binding.tvAge.text = it }
            binding.imgProfilePicture.setBitmapImage(employee.photo, true)
            binding.imgRemoveItem.setOnClickListener { onClick(employee, R.id.img_remove_item, position) }
            binding.rlParent.setOnClickListener { onClick(employee, R.id.rl_parent, position) }
        }
    }

    companion object {
        private val TAG = EmployeeListAdapter::class.java.simpleName
    }
}