package com.musta.employeelist.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employee_table")
class Employee(@field:ColumnInfo(name = "name") var name: String, @field:ColumnInfo(name = "age") var age: Int, @field:ColumnInfo(name = "gender") var gender: String, @field:ColumnInfo(name = "photo") var photo: ByteArray) {
    @PrimaryKey(autoGenerate = true)
    var id = 0
}