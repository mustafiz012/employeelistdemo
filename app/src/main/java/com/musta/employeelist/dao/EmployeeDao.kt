package com.musta.employeelist.dao

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.room.*
import com.musta.employeelist.model.Employee

@Dao
interface EmployeeDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(employee: Employee?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(employee: List<Employee>?)

    @Delete
    fun delete(employee: Employee)

    @Update
    fun update(employee: Employee)

    @Query("DELETE FROM employee_table")
    @WorkerThread
    fun deleteAll()

    @Query("SELECT * FROM employee_table WHERE  id = :id")
    fun findById(id: Int): LiveData<Employee>

    @get:Query("SELECT * FROM  employee_table ORDER BY name ASC")
    val alphabetizedEmployeeList: LiveData<List<Employee>>
}