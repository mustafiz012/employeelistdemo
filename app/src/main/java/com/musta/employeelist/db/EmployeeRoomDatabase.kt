package com.musta.employeelist.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.musta.employeelist.dao.EmployeeDao
import com.musta.employeelist.model.Employee
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Database(entities = [Employee::class], version = 2, exportSchema = false)
abstract class EmployeeRoomDatabase : RoomDatabase() {
    abstract fun employeeDao(): EmployeeDao

    companion object {
        private const val NUMBER_OF_THREAD = 4
        val databaseWriterExecutor: ExecutorService = Executors.newFixedThreadPool(NUMBER_OF_THREAD)

        @Volatile
        private var instance: EmployeeRoomDatabase? = null

        fun getDatabase(context: Context): EmployeeRoomDatabase =
                instance ?: synchronized(this) {
                    instance ?: buildDatabase(context).also { instance = it }
                }

        private fun buildDatabase(appContext: Context) =
                Room.databaseBuilder(appContext, EmployeeRoomDatabase::class.java, "employee_database")
                        .fallbackToDestructiveMigration()
                        .build()
    }
}