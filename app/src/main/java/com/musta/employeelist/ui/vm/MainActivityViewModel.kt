package com.musta.employeelist.ui.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.musta.employeelist.model.Employee
import com.musta.employeelist.repository.EmployeeRepository

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: EmployeeRepository = EmployeeRepository(application)

    val allEmployeeList: LiveData<List<Employee>>

    fun deleteDB() {
        repository.deleteAll()
    }

    fun insertDB(employeeList: List<Employee>) {
        repository.insertDB(employeeList)
    }

    init {
        allEmployeeList = repository.allEmployeeList
    }
}