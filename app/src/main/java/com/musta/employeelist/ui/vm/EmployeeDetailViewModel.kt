package com.musta.employeelist.ui.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.musta.employeelist.model.Employee
import com.musta.employeelist.repository.EmployeeRepository

class EmployeeDetailViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: EmployeeRepository = EmployeeRepository(application)

    private val _id = MutableLiveData<Int>()
    private val _employee = _id.switchMap { id -> repository.findById(id) }
    val employee: MutableLiveData<Employee> = _employee as MutableLiveData<Employee>

    fun start(employeeId: Int) {
        _id.value = employeeId
    }
}