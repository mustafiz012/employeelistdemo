package com.musta.employeelist.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.musta.employeelist.R
import com.musta.employeelist.databinding.ActivityMainBinding
import com.musta.employeelist.model.Employee
import com.musta.employeelist.ui.vm.MainActivityViewModel
import java.io.BufferedReader
import java.io.FileOutputStream
import java.io.InputStream
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {
    private val viewModel: MainActivityViewModel by viewModels()
    private var employeeList: List<Employee>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navHostFragment: NavHostFragment =
                supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController: NavController = navHostFragment.navController

        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.addEmployeeFragment -> if (navController.previousBackStackEntry!!.destination.id == R.id.employeeDetailFragment) binding.toolbar.title = getString(R.string.update_employee_title)
            }
        }

        viewModel.allEmployeeList.observe(this, {
            if (it.isNotEmpty()) employeeList = it
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_export -> {
                exportAsJson()
                true
            }
            R.id.action_import -> {
                importFromJson()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun importFromJson() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "text/*"
        startActivityForResult(intent, READ_REQ)
    }

    private fun exportAsJson() {
        if (employeeList.isNullOrEmpty()) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show()
            return
        }
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TITLE, "EmployeeData.txt")
        startActivityForResult(intent, WRITE_REQ)
    }

    private fun saveJsonToFile(uri: Uri) {
        try {
            val fileDescriptor = this.contentResolver.openFileDescriptor(uri, "w")!!
            val fileOutputStream = FileOutputStream(fileDescriptor.fileDescriptor)
            fileOutputStream.write(Gson().toJson(employeeList, object : TypeToken<List<Employee?>?>() {}.type).toByteArray())
            fileOutputStream.close()
            fileDescriptor.close()
            Toast.makeText(this@MainActivity, "Exported successfully", Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun readTextFile(uri: Uri) {
        val inputStream: InputStream?
        try {
            inputStream = contentResolver.openInputStream(uri)
            val reader = BufferedReader(InputStreamReader(inputStream))
            val total: StringBuilder = StringBuilder()
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                total.append(line).append('\n')
            }
            if (total.isNotEmpty()) {
                val response = Gson().fromJson<List<Employee>>(total.toString(), object : TypeToken<List<Employee?>?>() {}.type)
                if (!response.isNullOrEmpty()) {
                    viewModel.deleteDB()
                    viewModel.insertDB(response)
                } else Toast.makeText(this@MainActivity, getString(R.string.not_found), Toast.LENGTH_SHORT).show()
            }
            reader.close()
            inputStream!!.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        const val WRITE_REQ = 1001
        const val READ_REQ = 1002
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (resultCode == RESULT_OK) {
            var uri: Uri? = null
            if (resultData != null) {
                uri = resultData.data
            }
            when (requestCode) {
                READ_REQ -> {
                    if (uri != null) {
                        readTextFile(uri)
                    }
                }
                WRITE_REQ -> {
                    if (uri != null) {
                        saveJsonToFile(uri)
                    } else Toast.makeText(this@MainActivity, "Failed to export", Toast.LENGTH_SHORT).show()

                }
            }
        }
    }
}