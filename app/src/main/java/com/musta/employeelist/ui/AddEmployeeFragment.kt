package com.musta.employeelist.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.github.dhaval2404.imagepicker.ImagePicker
import com.musta.employeelist.R
import com.musta.employeelist.databinding.FragmentAddEmployeeBinding
import com.musta.employeelist.model.Employee
import com.musta.employeelist.ui.vm.AddEmployeeViewModel
import com.musta.employeelist.utils.setBitmapImage
import java.io.ByteArrayOutputStream
import java.util.*


class AddEmployeeFragment : Fragment() {
    private lateinit var values: Array<String>
    private var spinnerPosition: Int = 0
    private lateinit var binding: FragmentAddEmployeeBinding
    private val viewModel: AddEmployeeViewModel by viewModels()
    private var employee: Employee? = null
    private var photoData: ByteArray? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentAddEmployeeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        values = resources.getStringArray(R.array.genders)

        arguments?.getInt("employeeId")?.let { viewModel.start(it) }

        viewModel.employee.observe(viewLifecycleOwner, {
            employee = it
            loadExistingData(employee)
        })

        val dataAdapter: ArrayAdapter<String?> = object : ArrayAdapter<String?>(requireActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, values) {}

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spGender.adapter = dataAdapter
        binding.spGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                spinnerPosition = position
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        binding.spGender.setSelection(spinnerPosition)

        viewModel.addEmployeeFormState.observe(viewLifecycleOwner, {
            if (it == null) return@observe
            binding.fabSave.isEnabled = it.isDataValid
            if (it.nameError != null) {
                binding.tilName.error = getString(it.nameError!!)
            } else binding.tilName.error = null
            if (it.ageError != null) {
                binding.tilAge.error = getString(it.ageError!!)
            } else binding.tilAge.error = null
            if (it.photoUriError != null) {
                Toast.makeText(context, getString(it.photoUriError!!), Toast.LENGTH_SHORT).show()
            }
        })

        val afterTextChangedListener: TextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                viewModel.addDataChanged(binding.etName.text.toString(),
                        binding.etAge.text.toString(), photoData)
            }
        }

        binding.etName.addTextChangedListener(afterTextChangedListener)
        binding.etAge.addTextChangedListener(afterTextChangedListener)

        //Storing image as ByteArray just for demonstration
        binding.fabSave.setOnClickListener {
            if (!binding.fabSave.isEnabled) return@setOnClickListener
            if (photoData == null) {
                Toast.makeText(context, getString(R.string.invalid_photo_uri), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            hideKeyboard(requireActivity())
            if (viewModel.addOrUpdate(employee, binding.etName.text.toString(), binding.etAge.text.toString().toInt(), values[spinnerPosition], photoData!!))
                NavHostFragment.findNavController(this@AddEmployeeFragment).navigateUp()
        }

        binding.fabAddPhoto.setOnClickListener {
            ImagePicker.with(this)
                    .crop()                    //Crop image(Optional), Check Customization for more option
                    .compress(1024)            //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                    .start()
        }
    }

    private fun loadExistingData(employee: Employee?) {
        if (employee != null) {
            photoData = employee.photo
            binding.etName.setText(employee.name)
            binding.etAge.setText(employee.age.toString())
            binding.spGender.setSelection(values.indexOf(employee.gender))
            binding.imvProfilePicture.setBitmapImage(employee.photo, true)
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onDetach() {
        super.onDetach()
        hideKeyboard(requireActivity())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                //Image Uri will not be null for RESULT_OK
                val photo = BitmapFactory.decodeStream(context?.contentResolver?.openInputStream(data?.data!!))
                val stream = ByteArrayOutputStream()
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val imageByteArray = stream.toByteArray()
                photoData = imageByteArray
                viewModel.addDataChanged(binding.etName.text.toString(),
                        binding.etAge.text.toString(), photoData)
                binding.imvProfilePicture.setBitmapImage(imageByteArray, true)
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }
}