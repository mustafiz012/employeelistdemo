package com.musta.employeelist.ui.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.musta.employeelist.R
import com.musta.employeelist.model.Employee
import com.musta.employeelist.repository.EmployeeRepository

class AddEmployeeViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: EmployeeRepository = EmployeeRepository(application)
    private val _id = MutableLiveData<Int>()
    private val _employee = _id.switchMap { id -> repository.findById(id) }
    internal val addEmployeeFormState = MutableLiveData<AddEmployeeFormState?>()

    fun addDataChanged(name: String?, age: String?, photoUri: ByteArray?) {
        when {
            name.isNullOrEmpty() -> {
                addEmployeeFormState.setValue(AddEmployeeFormState(R.string.invalid_input, null, null))
            }
            age.isNullOrEmpty() || age.toInt() < 1 -> {
                addEmployeeFormState.setValue(AddEmployeeFormState(null, R.string.invalid_input, null))
            }
            /*photoUri == null -> {
                addEmployeeFormState.setValue(AddEmployeeFormState(null, null, R.string.invalid_photo_uri))
            }*/
            else -> {
                addEmployeeFormState.setValue(AddEmployeeFormState(true))
            }
        }
    }

    val employee: MutableLiveData<Employee> = _employee as MutableLiveData<Employee>

    val allEmployeeList: LiveData<List<Employee>>

    private fun insert(employee: Employee) {
        repository.insert(employee)
    }

    private fun update(employee: Employee) {
        repository.update(employee)
    }

    fun delete(employee: Employee) {
        repository.remove(employee)
    }

    init {
        allEmployeeList = repository.allEmployeeList
    }

    fun start(employeeId: Int) {
        _id.value = employeeId
    }

    fun addOrUpdate(employee: Employee?, name: String, age: Int, gender: String, photoUri: ByteArray): Boolean {
        if (employee == null) insert(Employee(name, age, gender, photoUri))
        else {
            employee.name = name
            employee.age = age
            employee.gender = gender
            employee.photo = photoUri
            update(employee)
        }
        return true
    }
}