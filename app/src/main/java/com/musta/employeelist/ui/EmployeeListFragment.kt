package com.musta.employeelist.ui

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.musta.employeelist.R
import com.musta.employeelist.adapter.EmployeeListAdapter
import com.musta.employeelist.databinding.FragmentEmployeeListBinding
import com.musta.employeelist.model.Employee
import com.musta.employeelist.ui.vm.AddEmployeeViewModel

class EmployeeListFragment : Fragment() {
    private lateinit var addEmployeeViewModel: AddEmployeeViewModel
    private var binding: FragmentEmployeeListBinding? = null

    private val adapter = EmployeeListAdapter { employee, id, position ->
        when (id) {
            R.id.img_remove_item -> {
                removeItem(employee, position)
            }
            R.id.rl_parent -> {
                findNavController().navigate(
                        R.id.action_employeeListFragment_to_employeeDetailFragment,
                        bundleOf("employeeId" to employee.id)
                )
            }
        }

    }

    private fun removeItem(employee: Employee, position: Int) {
        addEmployeeViewModel.delete(employee)
        adapter.notifyItemRemoved(position)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentEmployeeListBinding.inflate(inflater)
        addEmployeeViewModel = ViewModelProvider(this).get(AddEmployeeViewModel::class.java)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.fabAdd.setOnClickListener {
            NavHostFragment.findNavController(this@EmployeeListFragment)
                    .navigate(R.id.action_employeeListFragment_to_addEmployeeFragment)
        }

        //RecyclerView setup
        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        addEmployeeViewModel.allEmployeeList.observe(viewLifecycleOwner, { employeeList: List<Employee> -> adapter.setList(employeeList) })
    }
}