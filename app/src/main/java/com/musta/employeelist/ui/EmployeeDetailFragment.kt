package com.musta.employeelist.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.musta.employeelist.R
import com.musta.employeelist.databinding.EmployeeDetailFragmentBinding
import com.musta.employeelist.model.Employee
import com.musta.employeelist.ui.vm.EmployeeDetailViewModel
import com.musta.employeelist.utils.setBitmapImage

class EmployeeDetailFragment : Fragment() {
    private lateinit var binding: EmployeeDetailFragmentBinding
    private val viewModel: EmployeeDetailViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = EmployeeDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt("employeeId")?.let { viewModel.start(it) }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.employee.observe(viewLifecycleOwner, {
            bindEmployee(it)
        })
    }

    private fun bindEmployee(employee: Employee) {
        binding.tvName.text = employee.name
        binding.tvAge.text = employee.age.toString()
        binding.tvGender.text = employee.gender
        binding.imvProfilePicture.setBitmapImage(employee.photo, true)
        binding.fabEdit.setOnClickListener {
            findNavController().navigate(
                    R.id.action_employeeDetailFragment_to_addEmployeeFragment,
                    bundleOf("employeeId" to employee.id)
            )
        }
    }
}
