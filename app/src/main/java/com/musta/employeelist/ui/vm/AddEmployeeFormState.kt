package com.musta.employeelist.ui.vm

class AddEmployeeFormState {
    var nameError: Int?
    var ageError: Int?
    var photoUriError: Int?
    var isDataValid: Boolean

    internal constructor(nameError: Int?, ageError: Int?, photoUriError: Int?) {
        this.nameError = nameError
        this.ageError = ageError
        this.photoUriError = photoUriError
        isDataValid = false
    }

    internal constructor(isDataValid: Boolean) {
        nameError = null
        ageError = null
        photoUriError = null
        this.isDataValid = isDataValid
    }
}